/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <sstream>
#include <ignition/math/Rand.hh>
#include <gazebo/physics/World.hh>
#include "WorldGenerator.hh"
#include <ros/ros.h>
#include <string>
#include <map>
#include <xmlrpcpp/XmlRpcValue.h> 
#include <array>
using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(RubblePlugin)

/////////////////////////////////////////////////
RubblePlugin::RubblePlugin()
{
}

/////////////////////////////////////////////////
void RubblePlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
  ros::NodeHandle nh;
  this->world = _world;
  std::map<std::string,  double> box, cylinder, sphere;
  XmlRpc::XmlRpcValue obstacles, goals;
  if(nh.getParam("obstacles", obstacles)){
    this->GenerateObstacles(obstacles);
  }

  if(nh.getParam("goals", goals)){
    this->GenerateGoals(goals);
  }
  if(!nh.hasParam("blank_world")){
    if(nh.getParam("box", box)){
	    this->MakeBoxes(box);
    }
    if(nh.getParam("cylinder", cylinder)){
	    this->MakeCylinders(cylinder);
    }
    if(nh.getParam("sphere", sphere)){
	    this->MakeSpheres(sphere);
    }
  }
}
void RubblePlugin::GenerateObstacles(  XmlRpc::XmlRpcValue obstacles){
  if(obstacles.getType() == XmlRpc::XmlRpcValue::Type::TypeArray && obstacles.size() > 0){
    if(obstacles[0].getType() == XmlRpc::XmlRpcValue::Type::TypeStruct && obstacles[0].size() > 0){
        for (int32_t i = 0; i < obstacles.size(); ++i){
            std::vector<std::string> paramNames = {"pos_x", "pos_y", "pos_z", "size_x", "size_y", "size_z"};
            std::map<std::string, double> obstacleParams;
            for (int32_t i2 = 0; i2 < paramNames.size(); ++i2){
              if(obstacles[i][paramNames[i2]].getType() == XmlRpc::XmlRpcValue::Type::TypeDouble){
                obstacleParams[paramNames[i2]] = double(double(obstacles[i][paramNames[i2]]));
              }else if(obstacles[i][paramNames[i2]].getType() == XmlRpc::XmlRpcValue::Type::TypeInt){
                obstacleParams[paramNames[i2]] = double(int(obstacles[i][paramNames[i2]]));
              }
            }
            double mass = 100;
            Obj obj;
            obj.pose.Pos().X() = obstacleParams["pos_x"];
            obj.pose.Pos().Y() = obstacleParams["pos_y"];
            obj.pose.Pos().Z() = obstacleParams["pos_z"];
            obj.pose.Rot().Euler(ignition::math::Vector3d(0,0,0));
            obj.size.X() = obstacleParams["size_x"];
            obj.size.Y() = obstacleParams["size_y"];
            obj.size.Z() = obstacleParams["size_z"];
            std::string name = "obstacle_" + std::to_string(i);
            this->MakeBox(name,obj.pose,obj.size,mass);
        }
      }
    }
  }
/////////////////////////////////////////////////
void RubblePlugin::Init()
{
}
void RubblePlugin::GenerateGoals(XmlRpc::XmlRpcValue goals){
  if(goals.getType() == XmlRpc::XmlRpcValue::Type::TypeArray && goals.size() > 0){
    if(goals[0].getType() == XmlRpc::XmlRpcValue::Type::TypeStruct && goals[0].size() > 0){
        for (int32_t i = 0; i < goals.size(); ++i){
            std::vector<std::string> paramNames = {"location_x", "location_y", "location_size_x", "location_size_y"};
            std::map<std::string, double> goalParams;
            for (int32_t i2 = 0; i2 < paramNames.size(); ++i2){
              if(goals[i][paramNames[i2]].getType() == XmlRpc::XmlRpcValue::Type::TypeDouble){
                goalParams[paramNames[i2]] = double(double(goals[i][paramNames[i2]]));
              }else if(goals[i][paramNames[i2]].getType() == XmlRpc::XmlRpcValue::Type::TypeInt){
                goalParams[paramNames[i2]] = double(int(goals[i][paramNames[i2]]));
              }
            }

          Obj obj;
          obj.pose.Pos().X() = goalParams["location_x"] - 0.005;
          obj.pose.Pos().Y() = goalParams["location_y"] + 0.005;
          obj.pose.Pos().Z() = double(-0.5);
       

          std::string name = "goal_" + std::to_string(i);
              double length = double(1);
              double radius = double(0.06);
          double mass = double(100);
            this->MakeGoal(name, obj.pose,length, radius, mass);
        }
      }
    }
  
}
void RubblePlugin::MakeSpheres(std::map<std::string,  double> sphere){
  //positions to spawn objects
  ignition::math::Vector3d bottomRight = ignition::math::Vector3d(sphere["bottom_right_x"],sphere["bottom_right_y"],sphere["bottom_right_z"]);
  ignition::math::Vector3d topLeft = ignition::math::Vector3d(sphere["top_left_x"], sphere["top_left_y"], sphere["top_left_z"]);
  //mass ranges
  double minMass = sphere["min_mass"];
  double maxMass = sphere["max_mass"];
  unsigned int count = sphere["count"];
  
  for (unsigned int i = 0; i < count; ++i)
  {
    //generate the position    
    Obj obj;
    obj.pose.Pos().X() = ignition::math::Rand::DblUniform(bottomRight.X(),
        topLeft.X());
    obj.pose.Pos().Y() = ignition::math::Rand::DblUniform(bottomRight.Y(),
        topLeft.Y());
    obj.pose.Pos().Z() = ignition::math::Rand::DblUniform(bottomRight.Z(),
        topLeft.Z());
    obj.pose.Rot().Euler(ignition::math::Vector3d(
        ignition::math::Rand::DblUniform(0.0, 3.1415),
        ignition::math::Rand::DblUniform(-0.1, 0.1),
        ignition::math::Rand::DblUniform(0.0, 3.1415)));
    //create the terrain object name
    std::string name = "sphere" + std::to_string(i);
	//get size ranges
  	double minRadius = sphere["min_radius"];
	double maxRadius = sphere["max_radius"];
        //generate sizes
        double radius = ignition::math::Rand::DblUniform(minRadius, maxRadius);
    double mass = ignition::math::Rand::DblUniform(minMass, maxMass);
	    this->MakeSphere(name, obj.pose, radius, mass);
}
}
void RubblePlugin::MakeCylinders(std::map<std::string,  double> cylinder){
  //positions to spawn objects
  ignition::math::Vector3d bottomRight = ignition::math::Vector3d(cylinder["bottom_right_x"],cylinder["bottom_right_y"],cylinder["bottom_right_z"]);
  ignition::math::Vector3d topLeft = ignition::math::Vector3d(cylinder["top_left_x"], cylinder["top_left_y"], cylinder["top_left_z"]);
  //mass ranges
  double minMass = cylinder["min_mass"];
  double maxMass = cylinder["max_mass"];
  unsigned int count = cylinder["count"];
  for (unsigned int i = 0; i < count; ++i)
  {
    //generate the position    
    Obj obj;
    obj.pose.Pos().X() = ignition::math::Rand::DblUniform(bottomRight.X(),
        topLeft.X());
    obj.pose.Pos().Y() = ignition::math::Rand::DblUniform(bottomRight.Y(),
        topLeft.Y());
    obj.pose.Pos().Z() = ignition::math::Rand::DblUniform(bottomRight.Z(),
        topLeft.Z());
    //obj.pose.Rot().Euler(ignition::math::Vector3d(
      //  ignition::math::Rand::DblUniform(0.0, 3.1415),
        //ignition::math::Rand::DblUniform(-0.1, 0.1),
        //ignition::math::Rand::DblUniform(0.0, 3.1415)));
    //create the terrain object name
    std::string name = "cylinder_" + std::to_string(i);
	//get size ranges
  	double minLength = cylinder["min_length"];
	double maxLength = cylinder["max_length"];
  	double minRadius = cylinder["min_radius"];
	double maxRadius = cylinder["max_radius"];
        //generate sizes
        double length = ignition::math::Rand::DblUniform(minLength, maxLength);
        double radius = ignition::math::Rand::DblUniform(minRadius, maxRadius);
    double mass = ignition::math::Rand::DblUniform(minMass, maxMass);
	    this->MakeCylinder(name, obj.pose,length, radius, mass);
  }
}
void RubblePlugin::MakeBoxes(std::map<std::string,  double> box){
  //positions to spawn objects
  ignition::math::Vector3d bottomRight = ignition::math::Vector3d(box["bottom_right_x"],box["bottom_right_y"],box["bottom_right_z"]);
  ignition::math::Vector3d topLeft = ignition::math::Vector3d(box["top_left_x"], box["top_left_y"], box["top_left_z"]);
  //mass ranges
  double minMass = box["min_mass"];
  double maxMass = box["max_mass"];
  //how many to make
  unsigned int count = box["count"];
  for (unsigned int i = 0; i < count; ++i)
  {
    //generate the position    
    Obj obj;
    obj.pose.Pos().X() = ignition::math::Rand::DblUniform(bottomRight.X(),
        topLeft.X());
    obj.pose.Pos().Y() = ignition::math::Rand::DblUniform(bottomRight.Y(),
        topLeft.Y());
    obj.pose.Pos().Z() = ignition::math::Rand::DblUniform(bottomRight.Z(),
        topLeft.Z());
    obj.pose.Rot().Euler(ignition::math::Vector3d(
        ignition::math::Rand::DblUniform(0.0, 3.1415),
        ignition::math::Rand::DblUniform(-0.1, 0.1),
        ignition::math::Rand::DblUniform(0.0, 3.1415)));
    //create the terrain object name
    std::string name = "box_" + std::to_string(i);
    double mass = ignition::math::Rand::DblUniform(minMass, maxMass);
	//get size ranges
  	ignition::math::Vector3d minSize = ignition::math::Vector3d(box["min_size_x"], box["min_size_y"], box["min_size_z"]);
  	ignition::math::Vector3d maxSize = ignition::math::Vector3d(box["max_size_x"], box["max_size_y"], box["max_size_z"]);
        //generate sizes
        obj.size.X() = ignition::math::Rand::DblUniform(minSize.X(), maxSize.X());
        obj.size.Z() = ignition::math::Rand::DblUniform(minSize.Z(), maxSize.Z());
        obj.size.Y() = ignition::math::Rand::DblUniform(minSize.Y(), maxSize.Y());
        // Make sure the bottom of the rubble piece is above the bottomRight.Z()
        // This will prevent ground penetration.
        if (obj.pose.Pos().Z() - obj.size.Z() * 0.5 < bottomRight.Z())
          obj.pose.Pos().Z() = bottomRight.Z() + obj.size.Z() * 0.5;
	//create and insert the box
	this->MakeBox(name, obj.pose, obj.size, mass);
  }
}

/////////////////////////////////////////////////
void RubblePlugin::MakeBox(const std::string &_name,
                           ignition::math::Pose3d &_pose,
                           ignition::math::Vector3d &_size,
                           const double _mass)
{
  std::ostringstream newModelStr;

  float sx = _size.X();
  float sy = _size.Y();
  float sz = _size.Z();

  newModelStr << "<sdf version='" << SDF_VERSION << "'>"
    "<model name='" << _name << "'>"
    "<allow_auto_disable>true</allow_auto_disable>"
    "<pose>" << _pose << "</pose>"
    "<link name='link'>"
      "<velocity_decay>"
        "<linear>0.01</linear>"
        "<angular>0.01</angular>"
      "</velocity_decay>"
      "<inertial><mass>" << _mass << "</mass>"
        "<inertia>"
        "<ixx>" << (1.0/12.0) * _mass * (sy*sy + sz*sz) << "</ixx>"
        "<iyy>" << (1.0/12.0) * _mass * (sz*sz + sx*sx) << "</iyy>"
        "<izz>" << (1.0/12.0) * _mass * (sx*sx + sy*sy) << "</izz>"
        "<ixy>" << 0.0 << "</ixy>"
        "<ixz>" << 0.0 << "</ixz>"
        "<iyz>" << 0.0 << "</iyz>"
        "</inertia>"
      "</inertial>"
      "<collision name='collision'>"
        "<geometry>"
          "<box><size>" << _size << "</size></box>"
        "</geometry>"
      "</collision>"
      "<visual name='visual'>"
        "<geometry>"
          "<box><size>" << _size << "</size></box>"
        "</geometry>"
      "</visual>"
    "</link>"
  "</model>"
  "</sdf>";
  
  this->world->InsertModelString(newModelStr.str());
}


/////////////////////////////////////////////////
void RubblePlugin::MakeSphere(const std::string &_name,
                           ignition::math::Pose3d &_pose,
                           const double &_radius,
                           const double _mass)
{
  std::ostringstream newModelStr;

  newModelStr << "<sdf version='" << SDF_VERSION << "'>"
    "<model name='" << _name << "'>"
    "<allow_auto_disable>true</allow_auto_disable>"
    "<pose>" << _pose << "</pose>"
    "<link name='link'>"
      "<velocity_decay>"
        "<linear>0.01</linear>"
        "<angular>0.01</angular>"
      "</velocity_decay>"
      "<inertial><mass>" << _mass << "</mass>"
        "<inertia>"
        "<ixx>" << 0.4 << "</ixx>"
        "<iyy>" << 0.4 << "</iyy>"
        "<izz>" << 0.2 << "</izz>"
        "<ixy>" << 0.0 << "</ixy>"
        "<ixz>" << 0.0 << "</ixz>"
        "<iyz>" << 0.0 << "</iyz>"
        "</inertia>"
      "</inertial>"
      "<collision name='collision'>"
        "<geometry>"
          "<sphere><radius>" << _radius << "</radius></sphere>"
        "</geometry>"
      "</collision>"
      "<visual name='visual'>"
        "<geometry>"
          "<sphere><radius>" << _radius << "</radius></sphere>"
        "</geometry>"
      "</visual>"
    "</link>"
  "</model>"
  "</sdf>";
  
  this->world->InsertModelString(newModelStr.str());
}
void RubblePlugin::MakeCylinder(const std::string &_name,
                           ignition::math::Pose3d &_pose,
                           const double &_length,
                           const double &_radius,
                           const double _mass)
{
  std::ostringstream newModelStr;
  newModelStr << "<sdf version='" << SDF_VERSION << "'>"
    "<model name='" << _name << "'>"
    "<allow_auto_disable>true</allow_auto_disable>"
    "<pose>" << _pose << "</pose>"
    "<link name='link'>"
      "<velocity_decay>"
        "<linear>0.01</linear>"
        "<angular>0.01</angular>"
      "</velocity_decay>"
      "<inertial><mass>" << _mass << "</mass>"
        "<inertia>"
        "<ixx>" << 0.4 << "</ixx>"
        "<iyy>" << 0.4 << "</iyy>"
        "<izz>" << 0.2 << "</izz>"
        "<ixy>" << 0.0 << "</ixy>"
        "<ixz>" << 0.0 << "</ixz>"
        "<iyz>" << 0.0 << "</iyz>"
        "</inertia>"
      "</inertial>"
      "<collision name='collision'>"
        "<geometry>"
	"<cylinder>"
          "<length>" << _length << "</length>"
          "<radius>" << _radius << "</radius>"
	  "</cylinder>"
        "</geometry>"
      "</collision>"
      "<visual name='visual'>"
        "<geometry>"
	"<cylinder>"
          "<length>" << _length << "</length>"
          "<radius>" << _radius << "</radius>"
	  "</cylinder>"
        "</geometry>"
      "</visual>"
    "</link>"
  "</model>"
  "</sdf>";
  
  this->world->InsertModelString(newModelStr.str());
}
void RubblePlugin::MakeGoal(const std::string &_name,
                           ignition::math::Pose3d &_pose,
                           const double &_length,
                           const double &_radius,
                           const double _mass)
{
  std::ostringstream newModelStr;
  newModelStr << "<sdf version='" << SDF_VERSION << "'>"
  "<model name='" << _name << "'>"
        "<static>true</static>"

    "<allow_auto_disable>true</allow_auto_disable>"
    "<pose>" << _pose << "</pose>"
    "<link name='link'>"
      "<velocity_decay>"
        "<linear>0.01</linear>"
        "<angular>0.01</angular>"
      "</velocity_decay>"
      "<inertial><mass>" << _mass << "</mass>"
        "<inertia>"
        "<ixx>" << 0.4 << "</ixx>"
        "<iyy>" << 0.4 << "</iyy>"
        "<izz>" << 0.2 << "</izz>"
        "<ixy>" << 0.0 << "</ixy>"
        "<ixz>" << 0.0 << "</ixz>"
        "<iyz>" << 0.0 << "</iyz>"
        "</inertia>"
      "</inertial>"
      "<collision name='collision'>"
        "<geometry>"
	"<cylinder>"
          "<length>" << _length << "</length>"
          "<radius>" << _radius << "</radius>"
	  "</cylinder>"
        "</geometry>"
        "<surface>"
           " <contact>"
             " <collide_without_contact>true</collide_without_contact>"
            "</contact>"
          "</surface>"
      "</collision>"
      "<visual name='visual'>"
        "<geometry>"
	"<cylinder>"
          "<length>" << _length << "</length>"
          "<radius>" << _radius << "</radius>"
	  "</cylinder>"
        "</geometry>"
      "</visual>"
    "</link>"
  "</model>"
  "</sdf>";
  
  this->world->InsertModelString(newModelStr.str());
}
