Package Setup
Copy package into the src folder of your catkin workspace
run catkin_make in the src folder

Running tests
To run the automated tests enter the command "rosrun thb30_project automateTesting.py" and follow the prompts.
To run a premade model ensure it has odometry and laserscan capability. See largeWheel4WheelDrive.xacro for an example. Ensure a launch file is created. See largeRobot4WD.launch for an example. Then run the launch file followed by "rosrun thb30_project runTests.py" to run a singular test.

Terrain Configuration template file. This structure must be followed. obstacles and goals can be empty if needed. If a shape isnt needed enter 0 into the count
box: {
  count: 500,
  bottom_right_x: 1,
  bottom_right_y: -0.5,
  bottom_right_z: 0,
  top_left_x: 4,
  top_left_y: 3.5,
  top_left_z: 0,
  min_size_x: 0.102,
  min_size_y: 0.102,
  min_size_z: 0.002,
  max_size_x: 0.649,
  max_size_y: 0.649,
  max_size_z: 0.019,
  min_mass: 0.1,
  max_mass: 0.2
}
cylinder: {
  count: 0,
  bottom_right_x: 1,
  bottom_right_y: -0.5,
  bottom_right_z: 0,
  top_left_x: 4,
  top_left_y: 3.5,
  top_left_z: 0,
  min_length: 0.001,
  max_length: 0.009,
  min_radius: 0.001,
  max_radius: 0.009,
  min_mass: 0.1,
  max_mass: 0.2
}
sphere: {
  count: 0,
  bottom_right_x: 1,
  bottom_right_y: -0.5,
  bottom_right_z: 0,
  top_left_x: 4,
  top_left_y: 3.5,
  top_left_z: 0,
  min_radius: 0.01,
  max_radius: 0.019,
  min_mass: 0.009,
  max_mass: 0.05
}
goals: [
{
        location_x: 4,
        location_y: 0,
        location_size_x: 1,
        location_size_y: 1,
},{
        location_x: 3,
        location_y: 3,
        location_size_x: 1,
        location_size_y: 1,
},{
        location_x: 1,
        location_y: 0,
        location_size_x: 1,
        location_size_y: 1,
},
]
obstacles: [
{
        pos_x: 2,
        pos_y: 0,
        pos_z: 0,
        size_x: 0.5,
        size_y: 0.5,
        size_z: 1,

}
]

Robot model template file. This file must not be editted by the user. This is hear is a pure backup.
 <?xml version="1.0" ?>
<robot name="automated_robot" xmlns:xacro="http://www.ros.org/wiki/xacro">
  <xacro:property name="wheel_radius" value="0.06" />
  <xacro:property name="chassis_length" value="0.05" />
    <xacro:property name="laser_x" value="0.225" />
    <xacro:property name="laser_z" value="0.05" />
  <xacro:macro name="diff_drive" params="radius name left right">
      <plugin filename="libgazebo_ros_diff_drive.so" name="differential_drive_controller_${name}">
        <rosDebugLevel>Debug</rosDebugLevel>
        <publishWheelTF>True</publishWheelTF> 
        <publishWheelJointState>true</publishWheelJointState> 
        <alwaysOn>true</alwaysOn>
        <updateRate>60</updateRate>
        <leftJoint>${left}</leftJoint>
        <rightJoint>${right}</rightJoint>
        <wheelDiameter>${radius*2}</wheelDiameter>
        <wheelSeparation>0.3</wheelSeparation>
        <wheelTorque>50</wheelTorque>
        <wheelAcceleration>1</wheelAcceleration>
        <commandTopic>cmd_vel</commandTopic> 
        <odometryTopic>odom</odometryTopic>
        <odometryFrame>odom</odometryFrame>
        <robotBaseFrame>link_chassis</robotBaseFrame>
      </plugin>
  </xacro:macro>
  <xacro:macro name="laser_sensor" params='laser_x laser_z'>
    <gazebo reference="hokuyo_link">
      <sensor type="gpu_ray" name="head_hokuyo_sensor">
        <pose>0 0 0 0 0 0</pose>
        <visualize>true</visualize>
        <update_rate>40</update_rate>
        <ray>
          <scan>
            <horizontal>
              <samples>720</samples>
              <resolution>1</resolution>
              <min_angle>-1.570796</min_angle>
              <max_angle>1.570796</max_angle> 
            </horizontal>
          </scan>
          <range>
            <min>0.05</min>
            <max>2.0</max>
            <resolution>0.01</resolution>
          </range>
          <noise>
            <type>gaussian</type>
            <!-- Noise parameters based on published spec for Hokuyo laser
                achieving "+-30mm" accuracy at range < 10m.  A mean of 0.0m and
                stddev of 0.01m will put 99.7% of samples within 0.03m of the true
                reading. -->
            <mean>0.0</mean>
            <stddev>0.01</stddev>
          </noise>
        </ray>
        <plugin name="gazebo_ros_head_hokuyo_controller" filename="libgazebo_ros_gpu_laser.so">
          <topicName>laserscan</topicName>
          <frameName>hokuyo_link</frameName>
        </plugin>
      </sensor>
    </gazebo>
    <joint name="hokuyo_joint" type="fixed">
      <axis xyz="0 1 0" />
      <origin xyz="${laser_x} 0 ${laser_z}" rpy="0 0 0"/> 
      <parent link="link_chassis"/>
      <child link="hokuyo_link"/>
    </joint>
    <link name="hokuyo_link">
      <collision>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
      <box size="0.05 0.05 0.04"/>
        </geometry>
      </collision>
 
      <visual>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
      <box size="0.05 0.05 0.04"/>
        </geometry>
        <material name="red"/>
      </visual>

      <inertial>
        <mass value="0.01" />
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <inertia  ixx="1e-3" ixy = "0" ixz = "0" iyy="1e-3" iyz = "0" izz="1e-3" />       
      </inertial>
    </link>
    <joint name="stand_joint" type="fixed">
      <axis xyz="0 1 0" />
      <origin xyz="${laser_x} 0 ${(laser_z) / 2}" rpy="0 0 0"/>
      <parent link="link_chassis"/>
      <child link="stand_link"/>
    </joint>
    <link name="stand_link">
      <collision>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
      <box size="0.05 0.05 ${laser_z}"/>
        </geometry>
      </collision>

      <visual>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
      <box size="0.05 0.05 ${laser_z}"/>
        </geometry>
        <material name="red"/>
      </visual>

      <inertial>
        <mass value="0.01" />
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <inertia  ixx="1e-3" ixy = "0" ixz = "0" iyy="1e-3" iyz = "0" izz="1e-3" />       
      </inertial>
    </link>
  </xacro:macro>
  <xacro:macro name="chassis" params="mass x y z">
    <link name="link_chassis">
      <!-- pose and inertial -->
      <pose>0 0 0.1 0 0 0</pose>
      <inertial>
        <mass value="${mass}"/>
        <origin rpy="0 0 0" xyz="0 0 0.1"/>
        <inertia  ixx="1e-3" ixy = "0" ixz = "0" iyy="1e-3" iyz = "0" izz="1e-3" />       
      </inertial> 
      <!-- body -->
      <collision name="collision_chassis">
        <geometry>
          <box size="${x} ${y} ${z}"/>
        </geometry>
      </collision>
      <visual>  
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <geometry>
          <box size="${x} ${y} ${z}"/>
        </geometry>
      </visual>
    </link>
  </xacro:macro>

  <xacro:macro name="wheel" params="index radius x_pos y_pos mass length height">
    <link name="link_wheel_${index}">
      <inertial>
        <mass value="${mass}"/>
        <origin rpy="0 1.5707 1.5707" xyz="0 0 0"/>
        <inertia  ixx="1e-3" ixy = "0" ixz = "0" iyy="1e-3" iyz = "0" izz="1e-3" />       
      </inertial>
        <collision name="link_wheel_collision_${index}">
        <origin rpy="0 1.5707 1.5707" xyz="0 0 0"/>
        <geometry>
          <cylinder length="${height}" radius="${radius}"/>
        </geometry>
      </collision>
      <visual name="link_wheel_visual_${index}">
        <origin rpy="0 1.5707 1.5707" xyz="0 0 0"/>
        <geometry>
          <cylinder length="${height}" radius="${radius}"/>
        </geometry>
      </visual>
    </link>
    
    <joint name="joint_wheel_${index}" type="continuous">
      <origin rpy="0 0 0" xyz="${x_pos} ${y_pos} 0"/>
      <child link="link_wheel_${index}"/>
      <parent link="link_chassis"/>
      <axis rpy="0 0 0" xyz="0 1 0"/>
      <limit effort="50" velocity="10"/>
      <joint_properties damping="0" friction="0"/>
    </joint>
  </xacro:macro>

  <!-- diff drive -->
      <gazebo>

      </gazebo>
  <!-- laser -->
  <xacro:laser_sensor laser_x="${laser_x}" laser_z="${laser_z}"/>
  <!-- chassis -->
  <xacro:chassis mass="0.5" x="${chassis_length}" y="0.3" z="0.07"/>
  <!-- wheels -->
</robot>
