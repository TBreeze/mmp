#!/usr/bin/env python3

import subprocess
import os
from time import sleep
import signal
import rospy
from datetime import date
from datetime import datetime
#user input
def wheel_Count():
    try:
        wheels = int(input('Wheel Count (even number):')) 
        if((wheels % 2) != 0):
            raise Exception('Entered an odd number.')
    except:
        print('A valid even integer is required')
        wheels = wheel_Count()
    return wheels
def wheel_radius():
    try:
        radius = float(input('Wheel Radius:')) 
    except:
        print('A valid float is required')
        radius = wheel_radius()
    return radius
def chassis_length(wheelRadius, wheelCount):
    minLength = (wheelRadius * 2) * ((wheelCount-2) / 2)
    try:
        length = float(input('Chassis Length (min-' + str(minLength) +'):')) 
        if(length < minLength):
            raise Exception('Length too small.')
    except:
        print('A valid float is required')
        length = chassis_length(wheelRadius, wheelCount)
    return length
def drive_Type():
    try:
        drive = str(input('Drive - rear front all:'))
        if drive != 'rear' and drive != 'front' and drive != 'all' :
            raise Exception('invalid drive type')
    except:
        print('Enter a valid option (rear, front, all)')
        drive = drive_Type()
    return drive
def show_Gazebo():
    try:
        show = str(input('Show gazebo simulation - yes/no:')) 
        if show != 'yes' and show != 'no':
            raise Exception('invalid gazebo show type')
    except:
        print('Enter yes or no')
        show = show_Gazebo()
    return show
def find_project_path():
    project_path_process = subprocess.Popen("find / -path '*/src/thb30_project' 2>&1 | grep -v 'Permission denied'", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = project_path_process.communicate()
    project_path = out.decode("utf-8") 
    if(not project_path):
        exit("Could not find project under namespace 'thb30_project/'. Exiting!")
    return project_path[:-1] #remove the new line char
#launch generation
def updateLaunchFile(showGazebo):
    project_path = find_project_path()
    f = open(project_path + "/launch/automatedWorld.launch","r")
    content = f.read()
    if(showGazebo == "yes"):
        content = content.replace('<arg name="gui" value="false"/>','<arg name="gui" value="true"/>')
        content = content.replace('<arg name="headless" value="true"/>','<arg name="headless" value="false"/>')
    else:
        content = content.replace('<arg name="gui" value="true"/>','<arg name="gui" value="false"/>')
        content = content.replace('<arg name="headless" value="false"/>','<arg name="headless" value="true"/>')
    fw = open(project_path + "/launch/automatedWorld.launch","w")   
    fw.write(content)
    print("updated 'automatedWorld.launch'")

def updateRobotModel(wheels, drive, wheelRadius, chassisLength):
    project_path = find_project_path()
    template = open(project_path + "/urdf/automatedRobotTemplate.xacro","r")
    content = template.read()
    #wheel radius
    content = content.replace('<xacro:property name="wheel_radius" value="0.06" />','<xacro:property name="wheel_radius" value="'+str(wheelRadius)+'" />')
    #drive type
    split = content.split('\n')    
    i = 0
    for item in split:
        if("<!-- diff drive -->" in item):
            break
        i = i + 1
    if(drive == "all"):
        ind = 0
        while(ind< (wheels/2)):
            split.insert(i + 2, '<xacro:diff_drive radius="${wheel_radius}" name="'+str(ind)+'" left="joint_wheel_'+ str((ind*2)+1) +'" right="joint_wheel_'+str((ind*2)+2)+'" />')
            ind = ind + 1
    elif(drive == 'rear'):
        split.insert(i + 2, '<xacro:diff_drive radius="${wheel_radius}" name="1" left="joint_wheel_1" right="joint_wheel_2" />')
    elif(drive=='front'):
        split.insert(i + 2, '<xacro:diff_drive radius="${wheel_radius}" name="1" left="joint_wheel_'+ str(int(wheels) - 1) +'" right="joint_wheel_'+ str(wheels) +'" />')
    seperator = '\n'
    content = seperator.join(split)
    #chassis length
    content = content.replace('<xacro:property name="chassis_length" value="0.05" />','<xacro:property name="chassis_length" value="'+str(chassisLength)+'" />')
    #wheels
    spacing = chassisLength - (wheelRadius * 2) #take away the end wheels
    start = 0-(chassisLength / 2)
    end = 0+(chassisLength / 2)
    split = content.split('\n')    
    i = 0
    for item in split:
        if("<!-- wheels -->" in item):
            break
        i = i + 1
    split.insert(i + 1, '<xacro:wheel index="'+str(2)+'" radius="${wheel_radius}" x_pos="'+str(start)+'" y_pos="-0.17" mass="0.2" length="0.04" height="0.04"/>')
    split.insert(i + 1, '<xacro:wheel index="'+str(1)+'" radius="${wheel_radius}" x_pos="'+str(start)+'" y_pos="0.17" mass="0.2" length="0.04" height="0.04"/>')
    split.insert(i + 1, '<xacro:wheel index="'+str(int(wheels))+'" radius="${wheel_radius}" x_pos="'+str(end)+'" y_pos="-0.17" mass="0.2" length="0.04" height="0.04"/>')
    split.insert(i + 1, '<xacro:wheel index="'+str(int(wheels) - 1)+'" radius="${wheel_radius}" x_pos="'+str(end)+'" y_pos="0.17" mass="0.2" length="0.04" height="0.04"/>')
    if(wheels > 4):
        available = start + end
        available_start = 0 -(available / 2)
        availableSpacing = available / ((wheels/2)-2)
        index=1
        while(index < ((wheels - 2) / 2)):
            split.insert(i + 1, '<xacro:wheel index="'+str((index*2)+2)+'" radius="${wheel_radius}" x_pos="'+str(available_start + (index*availableSpacing))+'" y_pos="-0.17" mass="0.2" length="0.04" height="0.04"/>')
            split.insert(i + 1, '<xacro:wheel index="'+str((index*2)+1)+'" radius="${wheel_radius}" x_pos="'+str(available_start + (index*availableSpacing))+'" y_pos="0.17" mass="0.2" length="0.04" height="0.04"/>')
            index = index + 1
    seperator = '\n'
    content = seperator.join(split)
    #laser

    content = content.replace('<xacro:property name="laser_x" value="0.225" />','<xacro:property name="laser_x" value="'+str((chassisLength/2) - 0.025) +'" />')
    content = content.replace('<xacro:property name="laser_z" value="0.05" />','<xacro:property name="laser_z" value="'+str(wheelRadius + 0.02) +'" />')

    f = open(project_path + "/urdf/automatedRobot.xacro","w")
    f.write(content)
    print("updated 'automatedRobot.xacro'")

def run():
    wheelCount = wheel_Count()#TODO
    wheelRadius = wheel_radius()
    driveType = drive_Type()
    chassisLength = chassis_length(wheelRadius, wheelCount)
    showGazebo = show_Gazebo()
    updateRobotModel(wheelCount, driveType, wheelRadius, chassisLength)
    updateLaunchFile(showGazebo)


    today = date.today()
    todayFormatted = today.strftime("%d_%m_%Y")
    time = datetime.now()
    current_time = time.strftime("%H_%M_%S")
    name = todayFormatted+'_'+current_time

    rospy.set_param('test_run', name)

    print('Testing with terrain.')
    blank_world = 0
    testSet = 0
    while testSet < 2:
        runCount = 0
        while runCount < 20:
            print("set " + str(testSet) + " run " + str(runCount))
            gazeboProcess = subprocess.Popen("roslaunch thb30_project automatedWorld.launch",stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid) #this is non blocking
            sleep(30)
            subprocess.run('rosrun thb30_project runTests.py', shell=True)
            os.killpg(os.getpgid(gazeboProcess.pid), signal.SIGTERM)
            sleep(30)
            runCount = runCount + 1
        blank_world = 1
        testSet = testSet + 1
        print('Testing without terrain.')
        rospy.set_param("blank_world", "1")
        sleep(5)
    print('Finished Testing.')

if __name__ == "__main__":
        try:
            run()
        except rospy.ROSInterruptException:
            pass
