#!/usr/bin/env python3

import rospy
import sys
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
import math
import csv
from datetime import date
from datetime import datetime
import os.path
import subprocess
import os
import time
import signal

current_location = None 
current_laser_ranges = None 
navigate_aim = 'goal' 
sensors_to_average = 3
obstacle_in_action = False
obstacle_turn_time = 0
goal_times = []
obstacle_side_on = False
def odometry_cb(msg):
    global current_location
    current_location = msg.pose.pose
def laserscan_cb(msg):
    global current_laser_ranges, sensors_to_average, navigate_aim, obstacle_in_action
    current_laser_ranges = msg.ranges
    direction = {'x': 0,'z': 0}
    if current_laser_ranges != None:
        averaged_values = get_front_average_values()
        if averaged_values < 0.2:
            navigate_aim = 'obstacle'
        elif obstacle_in_action == False:
            navigate_aim = 'goal'

def get_front_average_values():
    global current_laser_ranges
    current_laser_ranges
    center_values = current_laser_ranges[int(len(current_laser_ranges) / 2)]
    i = 0
    while  i < sensors_to_average:
        center_values = current_laser_ranges[int(len(current_laser_ranges) / 2) + 1]
        i = i + 1
    i=0
    while  i < sensors_to_average:
        center_values = current_laser_ranges[int(len(current_laser_ranges) / 2) - 1]
        i = i + 1
    averaged_values = center_values / ((sensors_to_average * 2) + 1)
    return averaged_values

def get_robot_orientation():
    global current_location
    orientations = [current_location.orientation.x,current_location.orientation.y,current_location.orientation.z,current_location.orientation.w]
    (roll, pitch, yaw) = euler_from_quaternion(orientations)
    degrees = math.degrees(yaw)
    return degrees 

def bearing_to_goal(goal, robot_location):
    radians = math.atan2(goal['location_y']-robot_location.position.y,  goal['location_x']-robot_location.position.x)
    degrees = math.degrees(radians)
    return degrees 

def get_z_velocity(orientation, bearing):
    if  orientation < bearing - 1: 
        return 1
    elif orientation > bearing + 1: 
        return -1
    else:
        return 0

def get_direction_to_goal(goal):
    global current_location
    direction = {'x': 0,'z': 0}
    if current_location != None:
        bearing = bearing_to_goal(goal, current_location)
        orientation = get_robot_orientation()
        direction['x'] = 1
        direction['z'] = get_z_velocity(orientation, bearing)
        if bearing - orientation > 30 or bearing - orientation < -30:
            direction['x'] = 0.2
    return direction

def navigate_obstacle(goal):
    global obstacle_in_action, current_laser_ranges, obstacle_side_on,current_location
    direction = {'x': 0,'z': 0}
    obstacle_in_action = True
    #turn left so obstacle is side on
    if(math.isinf(current_laser_ranges[50]) and not obstacle_side_on):
        direction['z']= 1
    elif not obstacle_side_on:
        obstacle_side_on = True
        #nothing
    elif obstacle_side_on and not math.isinf(current_laser_ranges[50]):
        direction['x']= 0.2
        direction['z']= 1
    elif obstacle_side_on and math.isinf(current_laser_ranges[50]):
        direction['z']= -1
        direction['x']= 0.2
    brearing = bearing_to_goal(goal, current_location)
    orientation = get_robot_orientation()
    if obstacle_side_on and obstacle_in_action:
        if brearing - orientation < 5 and brearing - orientation > -5:
            obstacle_in_action = False
            obstacle_side_on = False
    # if obstacle_side_on:
    #     averaged_values = get_front_average_values()
    #     if averaged_values < 0.1:
    #         obstacle_side_on = False
    return direction

def get_goal_met(goal):
    global current_location
    goal_met = False
    if current_location != None:
        if not (current_location.position.x < (goal['location_x'] - (goal['location_size_x'] / 2))) and not (current_location.position.x > (goal['location_x'] + (goal['location_size_x'] / 2))) and not (current_location.position.y < (goal['location_y'] - (goal['location_size_y'] / 2))) and not (current_location.position.y > (goal['location_y'] + (goal['location_size_y'] / 2))): 
            goal_met = True
    return goal_met

def finish(pub, start_time, fail):
    publishMovement(pub,{'x': 0,'z': 0})
    total_time = rospy.get_time() - start_time
    write_to_csv(total_time, fail)
    if(not fail):
        print("Total time to complete goals = " + str(total_time))
        print("All goals completed.")
    else:
        print("Failure time = " + str(total_time))
        print("Failed tests.")

def run():
        global navigate_aim, goal_times
        rospy.init_node('move_node')
        rospy.Subscriber('odom', Odometry, odometry_cb)
        rospy.Subscriber('laserscan', LaserScan, laserscan_cb)

        pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        print("Started testing.")
        start_time = rospy.get_time()
        goal_start = start_time
        rate = rospy.Rate(20)
        goals = rospy.get_param('goals')
        fail = False
        for goal in goals:
            goal_met = False
            while not rospy.is_shutdown() and not goal_met and not fail:
                if navigate_aim == "goal":
                    direction = get_direction_to_goal(goal)
                elif navigate_aim == "obstacle":
                    direction = navigate_obstacle(goal)
                else:
                    rospy.logerror("ERROR no navigation aim")
                goal_met = get_goal_met(goal)
                publishMovement(pub, direction)
                rate.sleep()
                goal_time = rospy.get_time() - goal_start
                if(goal_time > 300):
                    fail = True
            if(goal_met):
                print("Goal completed. Time taken = " + str(goal_time))
                goal_start = rospy.get_time()
                goal_times.append(goal_time)                
        finish(pub,start_time, fail)


def publishMovement(pub, direction):
    msg = Twist()
    msg.linear.x = direction['x']
    msg.angular.z = direction['z']
    pub.publish(msg)
def find_project_path():
    project_path_process = subprocess.Popen("find / -path '*/src/thb30_project' 2>&1 | grep -v 'Permission denied'", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = project_path_process.communicate()
    project_path = out.decode("utf-8") 
    if(not project_path):
        exit("Could not find project under namespace 'thb30_project/'. Exiting!")
    return project_path[:-1] #remove the new line char

def write_to_csv(total_time, fail):
    global goal_times
    results = []
    today = date.today()
    todayFormatted = today.strftime("%d_%m_%Y")
    results.append(todayFormatted)
    time = datetime.now()
    current_time = time.strftime("%H:%M:%S")
    results.append(current_time)
    results.append(not rospy.has_param('blank_world'))
    results = results + goal_times
    goals = rospy.get_param('goals')
    if(fail):
        index = len(goal_times)
        while(index < len(goals)):
            results.append('Fail')
            index = index + 1

    results.append(total_time)
    results.append(str(not fail))

    if(rospy.has_param("test_run")):
        name = rospy.get_param("test_run")
    else:
        name = todayFormatted+'_'+current_time

    rowFound = False
    project_path = find_project_path()
    file_exists =  os.path.isfile(project_path + "/data/"+ name +".csv")
    if file_exists:
        with open(project_path + "/data/"+ name +".csv","r") as f:
            reader = csv.reader(f,delimiter = ",")
            data = list(reader)
            row_count = len(data)
            if row_count > 0:
                rowFound = True

    with open(project_path + "/data/"+ name +".csv", "a", newline='') as csvfile:
        csvWriter = csv.writer(csvfile, delimiter=',')
        if(not rowFound):
            header = ["Date", "Time", 'Terrain']
            goalIndex = 0
            while(goalIndex < len(goals)):
                header.append("Goal_" + str(goalIndex) + "_time")
                goalIndex = goalIndex + 1
            header.append("Total_time")
            header.append("Success")
            csvWriter.writerow(header)
        csvWriter.writerow(results)

if __name__ == "__main__":
        try:
            run()
        except rospy.ROSInterruptException:
            pass




















